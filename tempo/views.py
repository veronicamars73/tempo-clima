from django.shortcuts import render
import requests
from .models import Cidade
from .forms import CidadeForm

# Create your views here.

def index(request):
    url = "https://community-open-weather-map.p.rapidapi.com/weather"

    if request.method == 'POST':
        form = CidadeForm(request.POST)
        form.save()

    form = CidadeForm()
    
    cidades = Cidade.objects.all()

    dados_cidades = []

    for cidade in cidades:
        querystring = {"id":"2172797","lang":"pt","units":"metric","mode":"xml%2C html","q":cidade}
        headers = {
          'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
          'x-rapidapi-key': "e8a6292771msh67289abb8e0572fp1e1f25jsn551312caa50c"
        }
        response = requests.request("GET", url, headers=headers, params=querystring).json()
        dados_cidade = {
          'nome': cidade,
          'descricao': response['weather'][0]['description'],
          'representacao': response['weather'][0]['icon'],
          'temperatura': response['main']['temp'],
        }
        dados_cidades.append(dados_cidade)
       

    print(dados_cidades)
    context = {"dados_cidades": dados_cidades, "form": form}
    return render(request, 'tempo/tempo.html', context)
